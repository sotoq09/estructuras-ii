
package proyectoii.ui.main;

import proyectoii.providers.FileManager;

public class MainPresenter {
    private IMainView view;

    public MainPresenter(IMainView view) {
        this.view = view;
    }
    
    /**
     * Saves the data in files.
     */
    public void saveData() {
        FileManager.saveToFile();
        view.onSaveDataFinished();
    }    
    
}
