
package proyectoii.ui.main;

/**
 * This specifies the contract between main window and the FileManager.
 */
public interface IMainView {
    
     /**
     * Called when the presenter finish to save the data in files. It's used to
     * know when we can close the window.
     */
    void onSaveDataFinished();
}
