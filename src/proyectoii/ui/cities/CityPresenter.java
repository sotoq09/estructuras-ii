package proyectoii.ui.cities;

import proyectoii.providers.GraphProvider;

/**
 * Listen user actions from the UI ({@link CityFrame}), retrieves the data and
 * updates UI as required.
 */
public class CityPresenter {

    private ICityView view; // Allows communication with the window

    /**
     * Creates a presenter for a ({@link CityFrame}).
     *
     * @param view interface for communication with the window
     */
    public CityPresenter(ICityView view) {
        this.view = view;
    }

    /**
     * Attempts to add a new city
     *
     * @param cityName
     */
    public void addCity(String cityName) {
        if (GraphProvider.getInstance().addCity(cityName)) {
            view.onCityAddSuccess();
        } else {
            view.onCityAddError();
        }
    }

}
