package proyectoii.ui.cities;

/**
 * This specifies the contract between city window and the graph provider.
 */
public interface ICityView {

    /**
     * Called when a new city is added successfully.
     */
    void onCityAddSuccess();

    /**
     * Called if the city can't be added.
     */
    void onCityAddError();

}
