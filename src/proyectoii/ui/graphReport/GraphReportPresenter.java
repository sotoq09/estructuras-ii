package proyectoii.ui.graphReport;

import proyectoii.providers.GraphProvider;

/**
 * Listen user actions from the UI ({@link GraphReportFrame}), retrieves the
 * data and updates UI as required.
 */
public class GraphReportPresenter {

    private IGraphReportView view; // Allows communication with the window

    /**
     * Creates a presenter for a ({@link GraphReportFrame}).
     *
     * @param view interface for communication with the window
     */
    public GraphReportPresenter(IGraphReportView view) {
        this.view = view;
    }

    /**
     * Attempts to get the graph's depth information
     */
    public void getDepth() {
        view.onGetDepthSuccess(GraphProvider.getInstance().getDepth());
    }

    /**
     * Attempts to get the graph's Breadth information
     */
    public void getBreadth() {
        view.onGetBreadthSuccess(GraphProvider.getInstance().getBreadth());
    }
}
