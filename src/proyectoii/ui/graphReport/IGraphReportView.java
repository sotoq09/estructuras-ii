package proyectoii.ui.graphReport;

public interface IGraphReportView {

    /**
     * Called when a clients ask for graph's report successfully.
     *
     * @param result result
     */
    void onGetDepthSuccess(String result);

    /**
     * Called when a clients ask for graph's report successfully.
     *
     * @param result result
     */
    void onGetBreadthSuccess(String result);

}
