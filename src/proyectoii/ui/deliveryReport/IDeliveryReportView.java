package proyectoii.ui.deliveryReport;

import java.util.Iterator;
import proyectoii.models.Delivery;

/**
 * This specifies the contract between delivery's reports window and the file
 * manager.
 */
public interface IDeliveryReportView {

    /**
     * Called when a clients ask for delivery's report successfully.
     * @param treeIterator
     */
    void onDeliveryReportSuccess(Iterator<Delivery> treeIterator);
}
