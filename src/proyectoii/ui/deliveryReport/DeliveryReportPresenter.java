package proyectoii.ui.deliveryReport;

import proyectoii.providers.DeliveriesProvider;

/**
 * Listen user actions from the UI ({@link DeliveryReportFrame}), retrieves the
 * data and updates UI as required.
 */
public class DeliveryReportPresenter {

    private IDeliveryReportView view; // Allows communication with the window

    /**
     * Creates a presenter for a ({@link DeliveryReportFrame}).
     *
     * @param view interface for communication with the window
     */
    public DeliveryReportPresenter(IDeliveryReportView view) {
        this.view = view;
    }

    /**
     * Attempts to get the delivery's information.
     */
    public void getDeliveriesInformation() {
        view.onDeliveryReportSuccess(DeliveriesProvider.getInstance()
                .getDeliveries().iterator());
    }
}
