
package proyectoii.ui.deliveries;

public interface IDeliveryView {
    
    /**
     * Called when a new delivery is added successfully.
     */    
    void onAddDeliverySuccess();
    
    /**
    * Called if the delivery can't be added.
    */
    void onAddDeliveryError();
}
