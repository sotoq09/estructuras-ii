package proyectoii.ui.deliveries;

import proyectoii.graph.CityVertex;
import proyectoii.models.User;
import proyectoii.models.Vehicle;
import proyectoii.providers.DeliveriesProvider;

/**
 * Listen user actions from the UI ({@link DeliveryFrame}), retrieves the data
 * and updates UI as required.
 */
public class DeliveryPresenter {

    private IDeliveryView view; // Allows communication with the window

    /**
     * Creates a presenter for a ({@link DeliveryFrame}).
     *
     * @param view interface for communication with the window
     */
    public DeliveryPresenter(IDeliveryView view) {
        this.view = view;
    }

    /**
     * Attempts to add a new delivery.
     *
     * @param id id
     * @param driver driver
     * @param vehicle vehicle
     * @param origin origin city
     * @param destiny destination city
     * @param weight wight of the cargo and vehicle
     */
    public void addDelivery(int id, User driver, Vehicle vehicle,
            CityVertex origin, CityVertex destiny, float weight) {
        if (DeliveriesProvider.getInstance().addDelivery(id, driver, vehicle,
                origin, destiny, weight)) {
            view.onAddDeliverySuccess();
        } else {
            view.onAddDeliveryError();
        }
    }
}
