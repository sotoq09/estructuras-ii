package proyectoii.ui.highways;

/**
 * This specifies the contract between highway window and the graph provider.
 */
public interface IHighwayView {

    /**
     * Called when a new highway is added successfully.
     */
    void onHighwayAddSuccess();

}
