package proyectoii.ui.highways;

import proyectoii.graph.CityVertex;
import proyectoii.providers.GraphProvider;

/**
 * Listen user actions from the UI ({@link HighwayFrame}), retrieves the data
 * and updates UI as required.
 */
public class HighwayPresenter {

    private IHighwayView view; // Allows communication with the window

    /**
     * Creates a presenter for a ({@link HighwayFrame}).
     *
     * @param view interface for communication with the window
     */
    public HighwayPresenter(IHighwayView view) {
        this.view = view;
    }

    /**
     * Attempts to add a {@link RouteArc} between the specified
     * {@link CityVertex}.
     *
     * @param originCity origin city
     * @param destinationCity destination city
     * @param distance distance between cities
     * @param maximunSpeed maximum speed of the route
     * @param heavyVehiclesPass if heavy vehicles are allowed
     */
    public void addHighway(CityVertex originCity, CityVertex destinationCity,
            float distance, float maximunSpeed, boolean heavyVehiclesPass) {

        GraphProvider.getInstance().addRouteArc(originCity, destinationCity,
                distance, maximunSpeed, heavyVehiclesPass);

        view.onHighwayAddSuccess();
    }

}
