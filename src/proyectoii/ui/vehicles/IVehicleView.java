
package proyectoii.ui.vehicles;

public interface IVehicleView {
 
    /**
     * Called when a new vehicle is added successfully.
     */    
    void onAddVehicleSuccess();
    
    /**
     * Called if the vehicle can't be added.
     */
    void onAddVehicleError();
}
