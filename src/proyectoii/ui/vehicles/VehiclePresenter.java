package proyectoii.ui.vehicles;

import proyectoii.providers.VehiclesProvider;

/**
 * Listen user actions from the UI ({@link VehicleFrame}), retrieves the data
 * and updates UI as required.
 */
public class VehiclePresenter {

    private IVehicleView view; // Allows communication with the window

    /**
     * Creates a presenter for a ({@link VehicleFrame}).
     *
     * @param view interface for communication with the window
     */
    public VehiclePresenter(IVehicleView view) {
        this.view = view;
    }

    /**
     * Attempts to add a new vehicle
     *
     * @param plateNumber of the new vehicle
     * @param weight of the vehicle
     */
    public void addVehicule(String plateNumber, float weight) {
        if (VehiclesProvider.getInstance().addVehicle(plateNumber, weight)) {
            view.onAddVehicleSuccess();
        } else {
            view.onAddVehicleError();
        }
    }

}
