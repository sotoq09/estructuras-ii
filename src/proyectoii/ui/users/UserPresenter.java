package proyectoii.ui.users;

import proyectoii.models.License;
import proyectoii.providers.ClientsProvider;

/**
 * Listen user actions from the UI ({@link UserFrame}), retrieves the data and
 * updates UI as required.
 */
public class UserPresenter {

    private IUserView view; // Allows communication with the window

    /**
     * Creates a presenter for a ({@link VehicleFrame}).
     *
     * @param view interface for communication with the window
     */
    public UserPresenter(IUserView view) {
        this.view = view;
    }

    /**
     * Attempts to add a new user.
     *
     *
     * @param id of the new user
     * @param userName of the new user
     * @param licenseType type of license
     * @param maxWeight maximum weight of license
     */
    public void addUser(int id, String userName, String licenseType,
            float maxWeight) {

        License license = new License(licenseType, maxWeight);
        if (ClientsProvider.getInstance().addClient(id, userName, license)) {
            view.onUserAddSuccess();
        } else {
            view.onUserAddError();
        }
    }

}
