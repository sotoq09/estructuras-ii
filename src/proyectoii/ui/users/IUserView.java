package proyectoii.ui.users;

/**
 * This specifies the contract between user window and the user provider.
 */
public interface IUserView {

    /**
     * Called when a new user is added successfully.
     */
    void onUserAddSuccess();

    /**
     * Called if the user can't be added.
     */
    void onUserAddError();
}
