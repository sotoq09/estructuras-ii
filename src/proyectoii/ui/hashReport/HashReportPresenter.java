package proyectoii.ui.hashReport;

import proyectoii.providers.ClientsProvider;

/**
 * Listen user actions from the UI ({@link HashReportFrame}), retrieves the data
 * and updates UI as required.
 */
public class HashReportPresenter {

    private IHashReportView view; // Allows communication with the window

    /**
     * Creates a presenter for a ({@link HashReportFrame}).
     *
     * @param view interface for communication with the window
     */
    public HashReportPresenter(IHashReportView view) {
        this.view = view;
    }

    /**
     * Attempts to get the hashtable information.
     */
    public void getHashInformation() {
        view.onHashReportSuccess(
                ClientsProvider.getInstance().getClients().values());
    }
}
