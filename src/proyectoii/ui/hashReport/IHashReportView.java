package proyectoii.ui.hashReport;

import java.util.Iterator;
import proyectoii.models.User;

/**
 * This specifies the contract between hash table window and the file manager.
 */
public interface IHashReportView {

    /**
     * Called when a clients ask for hashtable successfully.
     */
    void onHashReportSuccess(Iterator<User> hashIterator);
}
