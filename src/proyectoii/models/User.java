package proyectoii.models;

import java.io.Serializable;

/**
 * Represents an user in the transport application.
 */
public class User implements Serializable {

    private int id;
    private String userName;
    private License license;

    /**
     * Constructor with all the attributes for User.
     *
     * @param id id of the user
     * @param userName user name
     * @param license type of license
     */
    public User(int id, String userName, License license) {
        this.id = id;
        this.userName = userName;
        this.license = license;
    }

    // Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }
}
