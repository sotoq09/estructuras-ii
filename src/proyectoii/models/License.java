package proyectoii.models;

import java.io.Serializable;

/**
 * Represents a type of license that the user can have.
 */
public class License implements Serializable {

    private String type;
    private float maximunWeight;

    /**
     * Constructor with all the attributes for License.
     *
     * @param type type of license
     * @param maximunWeight maximum weight that the license allows to carry
     */
    public License(String type, float maximunWeight) {
        this.type = type;
        this.maximunWeight = maximunWeight;
    }

    // Getters and setters
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getMaximunWeight() {
        return maximunWeight;
    }

    public void setMaximunWeight(float maximunWeight) {
        this.maximunWeight = maximunWeight;
    }
}
