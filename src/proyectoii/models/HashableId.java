package proyectoii.models;

import java.io.Serializable;
import proyectoii.util.Hasheable;

/**
 * The client id with hash implementation. This class will be the key for the
 * {@link HashMap}.
 */
public class HashableId implements Hasheable, Serializable {

    public int clientId;

    /**
     * Creates a new hashable id.
     *
     * @param id id
     */
    public HashableId(int id) {
        this.clientId = id;
    }

    /**
     * Generates a hash value for an integer. The value will be generated by
     * converting the int to String an by multiplying the last generated value
     * by 31 and then adding the next character.
     *
     * @return a hash value for the integer clientId
     */
    @Override
    public int getHash() {
        if (clientId == 0) {
            return 0;
        }

        String str = String.valueOf(clientId);
        int hash = 0;
        for (int i = 0; i < str.length(); i++) {
            hash = hash * 31 + str.charAt(i);
        }
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof HashableId) {
            return ((HashableId) obj).clientId == this.clientId;
        }
        return false;
    }
}
