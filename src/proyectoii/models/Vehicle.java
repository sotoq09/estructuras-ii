package proyectoii.models;

import java.io.Serializable;

/**
 * Represents a vehicle that can be drive by an user.
 */
public class Vehicle implements Serializable {

    private String plateNumber;
    private float weight;

    /**
     * Constructor with all the attributes for Vehicle.
     *
     * @param plateNumber number of plate
     * @param weight vehicle's weight
     */
    public Vehicle(String plateNumber, float weight) {
        this.plateNumber = plateNumber;
        this.weight = weight;
    }

    // Getters and setters
    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
