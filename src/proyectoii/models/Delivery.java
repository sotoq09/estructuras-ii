package proyectoii.models;

import java.io.Serializable;
import proyectoii.graph.CityVertex;

/**
 * Represents a delivery in the transport application.
 */
public class Delivery implements Comparable<Delivery>, Serializable {

    private int id;
    private User driver;
    private Vehicle vehicle;
    private CityVertex originCity;
    private CityVertex destCity;
    private float cargoWeight;

    /**
     * Creates a new delivery with all parameters.
     *
     * @param id id of the delivery
     * @param driver driver
     * @param vehicle driver's vehicle
     * @param originCity origin city
     * @param destCity destination city
     * @param cargoWeight cargo weight
     */
    public Delivery(int id, User driver, Vehicle vehicle, CityVertex originCity,
            CityVertex destCity, float cargoWeight) {
        this.id = id;
        this.driver = driver;
        this.vehicle = vehicle;
        this.originCity = originCity;
        this.destCity = destCity;
        this.cargoWeight = cargoWeight;
    }

    // Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public CityVertex getOriginCity() {
        return originCity;
    }

    public void setOriginCity(CityVertex originCity) {
        this.originCity = originCity;
    }

    public CityVertex getDestCity() {
        return destCity;
    }

    public void setDestCity(CityVertex destCity) {
        this.destCity = destCity;
    }

    public float getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(float cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    @Override
    public int compareTo(Delivery o) {
        return id - o.id;
    }
}
