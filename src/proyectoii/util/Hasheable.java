package proyectoii.util;

/**
 * Defines if an object can be hashed with custom implementation. Required by
 * the {@link HashMap} class to work
 */
public interface Hasheable {

    /**
     * @return the hash for the specified object
     */
    int getHash();
}
