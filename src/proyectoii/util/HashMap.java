package proyectoii.util;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Class that represents a HashMap.
 *
 * @param <K> generic type for key
 * @param <V> generic type for value
 */
public class HashMap<K extends Hasheable, V> implements Serializable {

    // An array with the map data
    private Node<K, V>[] mapEntries;

    private int currentCapacity;
    private int size;
    private float loadFactor;

    /**
     * Creates a HashMap with a default capacity for 10 elements and default
     * load factor of 0.75f.
     */
    public HashMap() {
        mapEntries = new Node[10];

        currentCapacity = 10;
        loadFactor = 0.75f;
        size = 0;
    }

    /**
     * @return the amount of elements in this HashTable
     */
    public int size() {
        return size;
    }

    /**
     * @return true if the hashtable doesn't have values
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Inserts the specified key and value to this hashtable. If there is
     * already an element for the key it's values will be overwritten.
     *
     * @param key key
     * @param value value
     */
    public void put(K key, V value) {
        int index = keyToIndex(key);
        Node<K, V> node = mapEntries[index];

        // Makes sure an element with the same key doesn't exists already
        while (node != null) {
            if (node.key.equals(key)) {
                node.value = value;
                return;
            }
            node = node.next;
        }

        // Let's insert the new element
        node = mapEntries[index];

        // Insertion at the begginnig
        Node<K, V> newNode = new Node<>(key, value, node);

        mapEntries[index] = newNode;
        size++;

        if (size / currentCapacity >= loadFactor) {
            rehash();
        }
    }

    /**
     * Retrieves the value for the specified key.
     *
     * @param key key
     * @return the value if exists
     */
    public V get(K key) {
        int index = keyToIndex(key);
        Node<K, V> node = mapEntries[index];

        while (node != null) {
            if (node.key.equals(key)) {
                return node.value;
            }
            node = node.next;
        }
        return null;
    }

    /**
     * Removes the key and its corresponding value from this hashtable.
     *
     * @param key key to remove
     * @return the removed value
     */
    public V remove(K key) {
        int index = keyToIndex(key);
        Node<K, V> prev = null;
        Node<K, V> node = mapEntries[index];

        while (node != null) {
            if (node.key.equals(key)) {
                if (prev != null) {
                    prev.next = node.next;
                } else {
                    mapEntries[index] = node.next;
                }
                size--;
                return node.value;
            }

            prev = prev.next;
            node = node.next;
        }
        return null;
    }

    /**
     * Converts the specified key to an array index.
     *
     * @param key key to convert
     * @return the index for the key
     */
    private int keyToIndex(K key) {
        return key.getHash() % currentCapacity;
    }

    /**
     * Internal function called to reorganize data after increment hashtable
     * size.
     */
    private void rehash() {
        currentCapacity *= 2;

        Node<K, V>[] oldMap = mapEntries;
        Node<K, V>[] newMap = new Node[currentCapacity];
        mapEntries = newMap;

        for (int i = 0; i < oldMap.length; i++) {
            Node<K, V> old = oldMap[i];
            while (old != null) {
                Node<K, V> updated = old;
                old = old.next;

                int newIndex = keyToIndex(updated.key);
                updated.next = newMap[newIndex];
                newMap[newIndex] = updated;
            }
        }
    }

    /**
     * Represents a node that composes each element of the HashTable.
     *
     * @param <K> generic type for key
     * @param <V> generic type for value
     */
    private static class Node<K extends Hasheable, V> implements Serializable {

        K key;
        V value;

        // Collision pointer
        Node<K, V> next;

        public Node(K key, V value, Node<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }

    /**
     * @return an iterator for the keys
     */
    public Iterator<K> keys() {
        return new HashIterator<>(KEYS);
    }

    /**
     * @return an iterator for the values
     */
    public Iterator<V> values() {
        return new HashIterator<>(VALUES);
    }

    // Types of iterator
    private static final int KEYS = 0;
    private static final int VALUES = 1;

    /**
     * Iterator class for this HashMap.
     *
     * @param <T> type of the iterator
     */
    private class HashIterator<T> implements Iterator<T> {

        int type;

        int index = 0;
        Node<K, V> node;

        public HashIterator(int type) {
            this.type = type;
        }

        @Override
        public boolean hasNext() {
            while (node == null && index < mapEntries.length) {
                node = mapEntries[index++];
            }

            return node != null;
        }

        @Override
        public T next() {
            Node<K, V> temp = node;
            node = node.next;
            return type == KEYS ? (T) temp.key : (T) temp.value;
        }

    }
}
