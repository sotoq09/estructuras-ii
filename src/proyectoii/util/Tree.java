package proyectoii.util;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Stack;

/**
 * A binary tree with sorted insertion. Elements must implement comparable
 * interface.
 *
 * @param <E> type of elements of the tree
 */
public class Tree<E extends Comparable> implements Serializable, Iterable<E> {

    private Node<E> root;

    /**
     * Creates a new tree.
     */
    public Tree() {
        this.root = null;
    }

    /**
     * Adds the desired element to the tree. Keeps the tree sorted.
     *
     * @param element element to be added
     */
    public void add(E element) {
        Node<E> newNode = new Node<>(element, null, null);

        if (this.root == null) {
            this.root = newNode;
        } else {
            Node<E> temp = this.root;
            while (true) {
                if (element.compareTo(temp.element) <= 0) {
                    if (temp.leftChild == null) {
                        temp.leftChild = newNode;
                        break;
                    }
                    temp = temp.leftChild;
                } else {
                    if (temp.rightChild == null) {
                        temp.rightChild = newNode;
                        break;
                    }
                    temp = temp.rightChild;
                }
            }
        }
    }

    /**
     * Removes the desired element from this tree.
     *
     * @param element element to remove
     * @return true if the element is in the tree and was removed
     */
    public boolean remove(E element) {
        // The tree is empty
        if (this.root == null) {
            return false;
        }

        // We need to remove the links from the parent
        Node<E> parentNode = null;

        Node<E> nodeToRemove = this.root;

        // Search the node to remove
        while (nodeToRemove != null) {
            // We found the element to remove
            if (element.compareTo(nodeToRemove.element) == 0) {
                // If the node to delete is the root
                if (parentNode == null) {
                    this.root = removeHelper(nodeToRemove);
                } // If the element is to the left of the parent
                else if (element.compareTo(parentNode.element) <= 0) {
                    parentNode.leftChild = removeHelper(nodeToRemove);
                } // If the element is to the right of the parent
                else {
                    parentNode.rightChild = removeHelper(nodeToRemove);
                }
                return true;
            } else if (element.compareTo(nodeToRemove.element) < 0) {
                parentNode = nodeToRemove;
                nodeToRemove = nodeToRemove.leftChild;
            } else {
                parentNode = nodeToRemove;
                nodeToRemove = nodeToRemove.rightChild;
            }
        }

        return false;
    }

    /**
     * Internal method. Acts as a helper for the remove method. When the remove
     * method founds the node to remove this is called and finishes the remove
     * operation.
     *
     * @param node node to remove
     * @return the new child node
     */
    private Node<E> removeHelper(Node<E> node) {
        // If has two children 
        if ((node.leftChild != null) && (node.rightChild != null)) {
            // Takes the right child and the moves to through the left till 
            // reach the end. Finally appends the left of the node to delete
            Node<E> newParentForLeft = node.rightChild;
            while (newParentForLeft.leftChild != null) {
                newParentForLeft = newParentForLeft.leftChild;
            }
            newParentForLeft.leftChild = node.leftChild;
            return node.rightChild;
        } // If has a child and is the right child
        else if ((node.rightChild != null)) {
            return node.rightChild;
        } //If has one child and is the left
        else if ((node.leftChild != null)) {
            return node.leftChild;
        }
        // If is a leaf
        return null;
    }

    /**
     * @return an iterator that allow this object to be traverse using a for
     * each.
     */
    @Override
    public Iterator<E> iterator() {
        return new InOrderIterator(this.root);
    }

    /**
     * Inner class that represents a node in this tree.
     *
     * @param <E> element of the node
     */
    private static class Node<E extends Comparable> implements Serializable {

        E element;
        Node<E> leftChild;
        Node<E> rightChild;

        /**
         * Creates a new node for this tree.
         *
         * @param element element inside the node
         * @param leftChild left child
         * @param rightChild right child
         */
        Node(E element, Node<E> leftChild, Node<E> rightChild) {
            this.element = element;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
        }
    }

    /**
     * An iterator that defines how to traverse this tree in a way that gets all
     * elements in order.
     */
    private class InOrderIterator implements Iterator<E> {

        private Stack<Node<E>> stack = new Stack<>();

        public InOrderIterator(Node<E> root) {
            pushToLeft(root);
        }

        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        @Override
        public E next() {
            Node<E> node = stack.pop();
            pushToLeft(node.rightChild);
            return node.element;
        }

        /**
         * Push in the stack all the nodes on the left starting from the
         * specified node till reach null.
         *
         * @param node start node
         */
        private void pushToLeft(Node<E> node) {
            while (node != null) {
                stack.push(node);
                node = node.leftChild;
            }
        }

    }
}
