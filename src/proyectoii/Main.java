package proyectoii;

import javax.swing.SwingUtilities;
import proyectoii.providers.FileManager;
import proyectoii.ui.main.MainWindow;

public class Main {

    public static void main(String[] args) {
        FileManager.loadFromFile();

        SwingUtilities.invokeLater(() -> {
            new MainWindow().setVisible(true);
        });
    }

}
