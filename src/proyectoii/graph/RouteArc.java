package proyectoii.graph;

import java.io.Serializable;

/**
 * Represent a route that connects {@link CityVertex} classes.
 */
public class RouteArc implements Serializable {

    private float distance;
    private float maximumSpeed;
    private boolean heavyVehiclesPass;
    private CityVertex destination;

    RouteArc nextArc;

    /**
     * Creates a RouteArc with the specified data.
     *
     * @param distance distance between vertexes
     * @param maximunSpeed maximum speed of the route
     * @param heavyVehiclesPass if heavy vehicles are allowed
     * @param destination the destination city
     */
    public RouteArc(float distance, float maximunSpeed,
            boolean heavyVehiclesPass, CityVertex destination) {
        this.distance = distance;
        this.maximumSpeed = maximunSpeed;
        this.heavyVehiclesPass = heavyVehiclesPass;
        this.destination = destination;
        this.nextArc = null;
    }

    // Getters and setters
    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getMaximunSpeed() {
        return maximumSpeed;
    }

    public void setMaximunSpeed(float maximunSpeed) {
        this.maximumSpeed = maximunSpeed;
    }

    public boolean heavyVehiclesPass() {
        return heavyVehiclesPass;
    }

    public void setHeavyVehiclesPass(boolean heavyVehiclesPass) {
        this.heavyVehiclesPass = heavyVehiclesPass;
    }

    public CityVertex getDestination() {
        return destination;
    }

    public void setDestination(CityVertex destination) {
        this.destination = destination;
    }
}
