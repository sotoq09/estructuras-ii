package proyectoii.graph;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A graph that represents a network of cities ({@link CityVertex}). Each city
 * is interconnected by routes ({@link RouteArc}).
 */
public class Graph implements Serializable {

    CityVertex firstVertex;

    /**
     * Creates a new graph.
     */
    public Graph() {
        this.firstVertex = null;
    }

    /**
     * Attempts to add a new city to this Graph.
     *
     * @param cityName name of the city
     * @return false if the city can't be added because there is already city
     * with the same name
     */
    public boolean addCityVertex(String cityName) {
        // If the city doesn't exists
        if (searchCityVertex(cityName) == null) {
            CityVertex newCity = new CityVertex(cityName);

            // Insert at the begginning
            newCity.nextVertex = this.firstVertex;
            this.firstVertex = newCity;
            return true;
        }

        return false;
    }

    /**
     * Searches the specified {@link CityVertex} by using it's name.
     *
     * @param cityName name of the city
     * @return the {@link CityVertex} if exists
     */
    public CityVertex searchCityVertex(String cityName) {
        CityVertex temp = this.firstVertex;

        while (temp != null) {
            if (temp.getCityName().equalsIgnoreCase(cityName)) {
                return temp;
            }
            temp = temp.nextVertex;
        }

        return null;
    }

    /**
     * Attempts to add a {@link RouteArc} between the specified
     * {@link CityVertex}.
     *
     * @param originCity origin city
     * @param destinationCity destination city
     * @param distance distance between cities
     * @param maximunSpeed maximum speed of the route
     * @param heavyVehiclesPass if heavy vehicles are allowed
     */
    public void addRouteArc(CityVertex originCity, CityVertex destinationCity,
            float distance, float maximunSpeed, boolean heavyVehiclesPass) {

        RouteArc newRoute = new RouteArc(distance, maximunSpeed,
                heavyVehiclesPass, destinationCity);

        // Insert the new route at the bigginnig of the origin city
        newRoute.nextArc = originCity.firstArc;
        originCity.firstArc = newRoute;
    }

    /**
     * Removes marks from visited cities;
     */
    private void removeMarks() {
        this.distance = 0;
        this.time = 0;
        this.route = "";

        CityVertex temp = this.firstVertex;

        while (temp != null) {
            temp.visited = false;
            temp = temp.nextVertex;
        }
    }

    private float distance = 0;
    private String route = "";
    private int time = 0;

    /**
     * Return a String which contains the closest route.
     *
     * @param origin city of origin
     * @param destination city of destination
     * @param cargoWeight height of the cargo
     * @return string with the closest route
     */
    public String getClosestRoute(CityVertex origin, CityVertex destination,
            float cargoWeight) {

        removeMarks();
        getClosestRouteHelper(origin, destination, cargoWeight, 0, 0, "");

        if (this.route.isEmpty()) {
            return null;
        }
        return route + ";" + distance + ";" + time;
    }

    private void getClosestRouteHelper(CityVertex origin, CityVertex destination,
            float cargoWeight, float distance, int time, String route) {

        if ((origin == null) || (origin.visited)) {
            return;
        }

        if (origin == destination) {
            if ((this.route.isEmpty()) || (this.distance > distance)) {
                this.route = route;
                this.distance = distance;
                this.time = time;
            }
        }

        if (origin != destination) {
            origin.visited = true;
        }

        RouteArc tempRouteArc = origin.firstArc;

        while (tempRouteArc != null) {
            if ((tempRouteArc.heavyVehiclesPass())
                    || ((!tempRouteArc.heavyVehiclesPass()) && (cargoWeight < 4000))) {

                float distanceMeters = tempRouteArc.getDistance() * 1000;
                float speedMeters = tempRouteArc.getMaximunSpeed() / 3.6f;

                getClosestRouteHelper(tempRouteArc.getDestination(),
                        destination,
                        cargoWeight,
                        distance + tempRouteArc.getDistance(),
                        time + (int) (time + distanceMeters / speedMeters),
                        route + origin.getCityName() + "-"
                        + tempRouteArc.getDestination().getCityName() + "|");
            }
            tempRouteArc.getDestination().visited = false;
            tempRouteArc = tempRouteArc.nextArc;
        }
    }

    /**
     * Return a String which contains the fastest route.
     *
     * @param origin city of origin
     * @param destination city of destination
     * @param cargoWeight height of the cargo
     * @return string with the fastest route
     */
    public String getFastestRoute(CityVertex origin, CityVertex destination,
            float cargoWeight) {

        removeMarks();
        getFastestRouteHelper(origin, destination, cargoWeight, 0, 0, "");

        if (this.route.isEmpty()) {
            return null;
        }
        return route + ";" + distance + ";" + time;
    }

    private void getFastestRouteHelper(CityVertex origin, CityVertex destination,
            float cargoWeight, float distance, int time, String route) {

        if ((origin == null) || (origin.visited)) {
            return;
        }

        if (origin == destination) {
            if ((this.route.isEmpty()) || (this.time > time)) {
                this.route = route;
                this.distance = distance;
                this.time = time;
            }
        }

        if (origin != destination) {
            origin.visited = true;
        }

        RouteArc tempRouteArc = origin.firstArc;

        while (tempRouteArc != null) {
            if ((tempRouteArc.heavyVehiclesPass())
                    || ((!tempRouteArc.heavyVehiclesPass()) && (cargoWeight < 4000))) {

                float distanceMeters = tempRouteArc.getDistance() * 1000;
                float speedMeters = tempRouteArc.getMaximunSpeed() / 3.6f;

                getFastestRouteHelper(tempRouteArc.getDestination(),
                        destination,
                        cargoWeight,
                        distance + tempRouteArc.getDistance(),
                        time + (int) (time + distanceMeters / speedMeters),
                        route + origin.getCityName() + "-"
                        + tempRouteArc.getDestination().getCityName() + "|");
            }
            tempRouteArc.getDestination().visited = false;
            tempRouteArc = tempRouteArc.nextArc;
        }
    }

    /**
     * @return all graph info
     */
    public String breadthRun() {
        StringBuilder builder = new StringBuilder();
        CityVertex tempCity = this.firstVertex;

        while (tempCity != null) {
            builder.append("City: ").append(tempCity.getCityName()).append("\n");

            RouteArc arc = tempCity.firstArc;
            while (arc != null) {
                builder.append(" ").append(tempCity.getCityName()).append(" - ")
                        .append(arc.getDestination().getCityName())
                        .append(" Distance: ").append(arc.getDistance())
                        .append(" km. Maximum speed: ")
                        .append(arc.getMaximunSpeed()).append("\n");

                arc = arc.nextArc;
            }

            tempCity = tempCity.nextVertex;
        }

        return builder.toString();
    }

    /**
     * @return all graph info
     */
    public String depthRun() {
        removeMarks();
        return depthRunHelper(this.firstVertex, new StringBuilder());
    }

    private String depthRunHelper(CityVertex city, StringBuilder builder) {
        if ((city == null) || (city.visited)) {
            return "";
        }

        city.visited = true;

        RouteArc arc = city.firstArc;
        while (arc != null) {
            builder.append(city.getCityName()).append(" - ")
                    .append(arc.getDestination().getCityName())
                    .append(" Distance: ").append(arc.getDistance())
                    .append(" km. Maximum speed: ")
                    .append(arc.getMaximunSpeed()).append("\n");
            depthRunHelper(arc.getDestination(), builder);
            arc = arc.nextArc;
        }

        return builder.toString();
    }

    /**
     * @return the cities representation in array to be used by the combo box
     */
    public ArrayList<CityVertex> getCitiesToArray() {
        ArrayList<CityVertex> list = new ArrayList<>();

        CityVertex temp = this.firstVertex;

        while (temp != null) {
            list.add(temp);
            temp = temp.nextVertex;
        }

        return list;
    }
}
