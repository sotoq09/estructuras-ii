package proyectoii.graph;

import java.io.Serializable;

/**
 * Represents a city for class {@link Graph}.
 */
public class CityVertex implements Serializable {

    private String cityName;

    CityVertex nextVertex;
    RouteArc firstArc;
    
    boolean visited;

    /**
     * Creates a city vertex with the specified city name.
     *
     * @param cityName name of the city
     */
    public CityVertex(String cityName) {
        this.cityName = cityName;
        this.nextVertex = null;
        this.firstArc = null;
        this.visited = false;
    }

    // Getters and setters
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
