package proyectoii.providers;

import proyectoii.graph.CityVertex;
import proyectoii.models.Delivery;
import proyectoii.models.User;
import proyectoii.models.Vehicle;
import proyectoii.util.Tree;

/**
 * Class used to manipulates all data related with the deliveries.
 */
public class DeliveriesProvider {

    private static DeliveriesProvider instance;
    private Tree<Delivery> deliveriesTree;

    /**
     * We are going to use the singleton pattern, so let's keep this private.
     */
    private DeliveriesProvider() {
        deliveriesTree = new Tree<>();
    }

    /**
     * @return the unique instance of the deliveries provider
     */
    public static synchronized DeliveriesProvider getInstance() {
        if (instance == null) {
            instance = new DeliveriesProvider();
        }
        return instance;
    }

    /**
     * Adds a new delivery if doesn't exists already.
     *
     * @param id id of the delivery
     * @param driver driver
     * @param vehicle driver's vehicle
     * @param originCity origin city
     * @param destCity destination city
     * @param cargoWeight cargo weight
     * @return false if the delivery already exists
     */
    public boolean addDelivery(int id, User driver, Vehicle vehicle,
            CityVertex originCity, CityVertex destCity, float cargoWeight) {
        if (getDelivery(id) == null) {
            deliveriesTree.add(new Delivery(id, driver, vehicle, originCity,
                    destCity, cargoWeight));
            return true;
        }
        return false;
    }

    /**
     * Searches the specified {@link Delivery} by using it's plate number.
     *
     * @param id id of the delivery
     * @return the {@link Delivery} if exists
     */
    public Delivery getDelivery(int id) {
        for (Delivery delivery : deliveriesTree) {
            if (delivery.getId() == id) {
                return delivery;
            }
        }

        return null;
    }

    /**
     * Allows {@link FileManager} load the deliveries on this provider. Internal
     * method, package visibility.
     *
     * @param deliveriesTree the tree to load.
     */
    void loadDeliveries(Tree<Delivery> deliveriesTree) {
        this.deliveriesTree = deliveriesTree;
    }

    /**
     * Allows {@link FileManager} save the deliveries on this provider.
     *
     * @return the deliveries tree.
     */
    public Tree<Delivery> getDeliveries() {
        return this.deliveriesTree;
    }
}
