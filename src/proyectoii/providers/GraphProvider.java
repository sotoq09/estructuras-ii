package proyectoii.providers;

import java.util.ArrayList;
import proyectoii.graph.CityVertex;
import proyectoii.graph.Graph;

/**
 * Class used to manipulates all data related with the graph (cities and
 * highways).
 */
public class GraphProvider {

    private static GraphProvider instance;
    private Graph cityGraph;

    /**
     * We are going to use the singleton pattern, so let's keep this private.
     */
    private GraphProvider() {
        this.cityGraph = new Graph();
    }

    /**
     * @return the unique instance of the graph provider
     */
    public static synchronized GraphProvider getInstance() {
        if (instance == null) {
            instance = new GraphProvider();
        }
        return instance;
    }

    /**
     * Adds a new city if doesn't exists already.
     *
     * @param cityName name of the city
     * @return false if the city already exists
     */
    public boolean addCity(String cityName) {
        if (getCity(cityName) == null) {
            cityGraph.addCityVertex(cityName);
            return true;
        }
        return false;
    }

    /**
     * Searches the specified {@link CityVertex} by using it's name.
     *
     * @param cityName name of the city
     * @return the {@link CityVertex} if exists
     */
    public CityVertex getCity(String cityName) {
        return cityGraph.searchCityVertex(cityName);
    }

    /**
     * Attempts to add a {@link RouteArc} between the specified
     * {@link CityVertex}.
     *
     * @param originCity origin city
     * @param destinationCity destination city
     * @param distance distance between cities
     * @param maximunSpeed maximum speed of the route
     * @param heavyVehiclesPass if heavy vehicles are allowed
     */
    public void addRouteArc(CityVertex originCity, CityVertex destinationCity,
            float distance, float maximunSpeed, boolean heavyVehiclesPass) {

        cityGraph.addRouteArc(originCity, destinationCity, distance,
                maximunSpeed, heavyVehiclesPass);
    }

    /**
     * Return a String which contains the closest route.
     *
     * @param origin city of origin
     * @param destination city of destination
     * @param cargoWeight height of the cargo
     * @return string with the closest route
     */
    public String getClosestRoute(CityVertex origin, CityVertex destination,
            float cargoWeight) {
        return cityGraph.getClosestRoute(origin, destination, cargoWeight);
    }
    
    /**
     * Return a String which contains the fastest route.
     *
     * @param origin city of origin
     * @param destination city of destination
     * @param cargoWeight height of the cargo
     * @return string with the fastest route
     */
    public String getFastestRoute(CityVertex origin, CityVertex destination,
            float cargoWeight) {
        return cityGraph.getFastestRoute(origin, destination, cargoWeight);
    }

    /**
     * @return the cities representation in array to be used by the combo box
     */
    public ArrayList<CityVertex> getCitiesToArray() {
        return cityGraph.getCitiesToArray();
    }

    /**
     * @return all graph info
     */
    public String getBreadth() {
        return cityGraph.breadthRun();
    }

    /**
     * @return all graph info
     */
    public String getDepth() {
        return cityGraph.depthRun();
    }

    /**
     * Allows {@link FileManager} load the graph on this provider. Internal
     * method, package visibility.
     *
     * @param cityGraph the graph to load.
     */
    void loadGraph(Graph cityGraph) {
        this.cityGraph = cityGraph;
    }

    /**
     * Allows {@link FileManager} save the graph on this provider. Internal
     * method, package visibility.
     *
     * @return the graph of the provider.
     */
    Graph getGraph() {
        return this.cityGraph;
    }
}
