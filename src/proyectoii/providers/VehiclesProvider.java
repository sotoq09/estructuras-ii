package proyectoii.providers;

import java.util.ArrayList;
import proyectoii.models.Vehicle;

/**
 * Class used to manipulates all data related with the vehicles.
 */
public class VehiclesProvider {

    private static VehiclesProvider instance;
    private ArrayList<Vehicle> vehiclesList;

    /**
     * We are going to use the singleton pattern, so let's keep this private.
     */
    private VehiclesProvider() {
        vehiclesList = new ArrayList<>();
    }

    /**
     * @return the unique instance of the graph provider
     */
    public static synchronized VehiclesProvider getInstance() {
        if (instance == null) {
            instance = new VehiclesProvider();
        }
        return instance;
    }

    /**
     * Adds a new vehicle if doesn't exists already.
     *
     * @param plateNumber plate number of the vehicle
     * @param weight weight of the vehicle
     * @return false if the vehicle already exists
     */
    public boolean addVehicle(String plateNumber, float weight) {
        if (getVehicle(plateNumber) == null) {
            vehiclesList.add(new Vehicle(plateNumber, weight));
            return true;
        }
        return false;
    }

    /**
     * Searches the specified {@link Vehicle} by using it's plate number.
     *
     * @param plateNumber plate number of the vehicle
     * @return the {@link Vehicle} if exists
     */
    public Vehicle getVehicle(String plateNumber) {
        for (Vehicle vehicle : vehiclesList) {
            if (vehicle.getPlateNumber().equals(plateNumber)) {
                return vehicle;
            }
        }

        return null;
    }

    /**
     * Allows {@link FileManager} load the vehicles on this provider. Internal
     * method, package visibility.
     *
     * @param vehiclesList the list to load.
     */
    void loadVehicles(ArrayList<Vehicle> vehiclesList) {
        this.vehiclesList = vehiclesList;
    }

    /**
     * Allows {@link FileManager} save the vehicles on this provider.
     *
     * @return the vehicles list.
     */
    public ArrayList<Vehicle> getVehicles() {
        return this.vehiclesList;
    }
}
