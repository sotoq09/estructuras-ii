package proyectoii.providers;

import proyectoii.models.HashableId;
import proyectoii.models.License;
import proyectoii.models.User;
import proyectoii.util.HashMap;

/**
 * Class used to manipulates all data related with the clients.
 */
public class ClientsProvider {

    private static ClientsProvider instance;
    private HashMap<HashableId, User> clientsMap;

    /**
     * We are going to use the singleton pattern, so let's keep this private.
     */
    private ClientsProvider() {
        clientsMap = new HashMap<>();
    }

    /**
     * @return the unique instance of the graph provider
     */
    public static synchronized ClientsProvider getInstance() {
        if (instance == null) {
            instance = new ClientsProvider();
        }
        return instance;
    }

    /**
     * Adds a new client if doesn't exists already.
     *
     * @param id id of the user
     * @param userName user name
     * @param license type of license
     * @return false if the client already exists
     */
    public boolean addClient(int id, String userName, License license) {
        if (getClient(id) == null) {
            clientsMap.put(new HashableId(id), new User(id, userName, license));
            return true;
        }
        return false;
    }

    /**
     * Searches the specified {@link Client} by using it's id.
     *
     * @param id id of the user
     * @return the {@link Client} if exists
     */
    public User getClient(int id) {
        return clientsMap.get(new HashableId(id));
    }

    /**
     * Allows {@link FileManager} load the clients on this provider. Internal
     * method, package visibility.
     *
     * @param clientsMap the clients to load.
     */
    void loadClients(HashMap<HashableId, User> clientsMap) {
        this.clientsMap = clientsMap;
    }

    /**
     * Allows {@link FileManager} save the clients on this provider.
     *
     * @return the clients map.
     */
    public HashMap<HashableId, User> getClients() {
        return this.clientsMap;
    }
}
