package proyectoii.providers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyectoii.graph.Graph;
import proyectoii.models.Delivery;
import proyectoii.models.HashableId;
import proyectoii.models.User;
import proyectoii.models.Vehicle;
import proyectoii.util.HashMap;
import proyectoii.util.Tree;

/**
 * Contains methods to save and load program data from files.
 */
public class FileManager {
    
    private static final String FILE_NAME = "program.dat";

    /**
     * Save all the data form the providers to the disk.
     */
    public static void saveToFile() {
        ObjectOutputStream objectOutput = null;
        
        try (FileOutputStream fileOutput = new FileOutputStream(FILE_NAME)) {
            objectOutput = new ObjectOutputStream(fileOutput);
            
            objectOutput.writeObject(GraphProvider.getInstance().getGraph());
            objectOutput.writeObject(ClientsProvider.getInstance().getClients());
            objectOutput.writeObject(VehiclesProvider.getInstance().getVehicles());
            objectOutput.writeObject(DeliveriesProvider.getInstance().getDeliveries());
            
            objectOutput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (objectOutput != null) {
                    objectOutput.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Load all data from the disk to the providers.
     */
    public static void loadFromFile() {
        ObjectInputStream objectInput = null;
        
        try (FileInputStream fileInput = new FileInputStream(FILE_NAME)) {
            objectInput = new ObjectInputStream(fileInput);
            
            Graph cities = (Graph) objectInput.readObject();
            GraphProvider.getInstance().loadGraph(cities);
            
            HashMap<HashableId, User> map = (HashMap) objectInput.readObject();
            ClientsProvider.getInstance().loadClients(map);
            
            ArrayList<Vehicle> vehicles = (ArrayList) objectInput.readObject();
            VehiclesProvider.getInstance().loadVehicles(vehicles);
            
            Tree<Delivery> deliveries = (Tree) objectInput.readObject();
            DeliveriesProvider.getInstance().loadDeliveries(deliveries);
            
            objectInput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (objectInput != null) {
                    objectInput.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
